# setup ATLAS
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase

source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh 

asetup Athena,22.0.41

if [ ! -d build ] ; then mkdir build ; fi
if [ ! -d run ] ; then mkdir run ; fi

# Add code to check if compilation was done
cd build
cmake ../source 
make -j10

source x*/setup.sh

cd ..
                                                                                                                                                                                                              

                                                             
