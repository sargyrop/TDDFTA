# TDDFTA

To run `training-dataset-dumper` with `FakeTrackAlgorithm`


### How to compile

```
source setup.sh
```


### How to run 

```
cd run
ca-dump-single-btag -c ../source/training-dataset-dumper/configs/single-b-tag/EMPFlowGNN.json -m 1000 /nfs/dust/atlas/user/sargyrop/FTAG/Roman/FTAG1_testLigang/DAOD_FTAG1.test.root
```

The file is one of the test `FTAG1` produced in [AFT-572](https://its.cern.ch/jira/browse/AFT-572).
